# Material Tag Suggest

Material Tag Suggest is a simple utility that allows users to select and add tags in a React project using Material-UI. The goal is to leverage native Material-UI components and provide as much extensibility as possible.

This project combines concepts from [React Tag Autocomplete](https://github.com/i-like-robots/react-tags) project by [I like robots](https://github.com/i-like-robots) and Kent C Dodds excellent [Star Wars Mailer](https://codesandbox.io/s/github/kentcdodds/downshift-examples/tree/master/?module=%2Fsrc%2Fordered-examples%2F04-multi-select.js&moduleview=1)

## Dependencies

This project is intended to be used within a React application that utilizes Material-UI. Before importing this component into your project make sure you have initalized a Reach App and bootstrapped a Material-UI provider.

---

## Installation

Material Tag Suggest is avaialble via npm.

```javascript
npm i material-tag-suggest

yarn add material-tag-suggest
```

## Usage

Include a sample component instantiation here

```javascipt

```

You can also take a look at the `/example` folder for a more complete implementation with minimal props provided to the component.

---

# Options/Props

The following options and props can be provided to the component

## Values

- [`id`](#id-optional)
- [`label`](#label)
- [`placeholder`](#placeholder-optional)
- [`minSuggestLength`](#minsuggestlength-optional)
- [`delimiters`](#delimiters-optional)
- []()

## Functions

- [`onAddTag`](#onaddtag-optional)
- [`onDeleteTag`](#ondeletetag-optional)
- [`onCreateTag`](#oncreatetag-optional)
- []()

## Values

#### id (optional)

The ID of the component
_Default value: 'material-tags'_

#### label

The label to use for the Input
_Default value: ''_

#### placeholder (optional)

The placeholder to use for the Input
_Default value: ''_

#### minSuggestLength (optional)

The minimum length input that will trigger an autosuggestion
_Default value: 3_

#### delimiters (optional)

An array containing string values for any key to use as a delimiter to create a new tag  
_Default value: []_

## Functions

#### onAddTag (optional)

Function that is called when a tag is selected and added to the collection
`Returns {addedTag, selectedTags}`

#### onDelete (optional)

Function to be called when a tag is deleted from the collection of tags
`Returns {deletedTag, selectedTags}`

#### onCreateTag (optional)

Function to be called when a new tag is created and added to the collection of tags
`Returns {createdTag, selectedTags}`

---

## Styling (`propClassNames`)

Material Tags Suggest provides base Styling but can also be custom styled. The core styling is based on Material UI's component styling and uses makeStyles and CSS in JS. To provide different styles pass in a `propClassNames` prop.

**_`propClassNames` must contain all styles including any base styles you intend to use._**

### **propClassNames** classes

- [`materialTags`](#materialtags)
- [`materialTagsFocused`](#materialtagsfocused)

#### materialTags

Styling for the wrapper component.

#### materialTagsFocused

Styles the wrapper when focused
