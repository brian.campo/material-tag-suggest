import React from 'react';
import PropTypes from 'prop-types';

import { Paper, MenuList, MenuItem } from '@material-ui/core';

function getSuggestions(props) {
  const { options, inputValue, selectedTags } = props;

  const optionLimit = 5;
  let count = 0;

  return options.filter(option => {
    const keep =
      count < optionLimit &&
      option.label.toLowerCase().includes(inputValue.toLowerCase()) &&
      !(selectedTags.find(tag => tag.label === option.label) !== undefined);
    keep ? (count += 1) : null;
    return keep;
  });
}

function renderSuggestions(props) {
  const { options, inputValue, selectedTags, getItemProps, highlightedIndex } = props;

  const items = getSuggestions({ options, inputValue, selectedTags }).map((suggestion, index) => {
    const isHighlighted = highlightedIndex === index;
    return (
      <MenuItem
        {...getItemProps({ item: suggestion })}
        dense
        key={suggestion.label}
        selected={isHighlighted}
        component='div'
      >
        {suggestion.label}
      </MenuItem>
    );
  });
  return items;
}

function Suggestion(props) {
  const {
    classes,
    menuProps,
    options,
    inputValue,
    selectedTags,
    getItemProps,
    highlightedIndex
  } = props;

  return (
    <div {...menuProps}>
      <Paper className={classes.paper} square style={{ width: 260 }}>
        <MenuList>
          {renderSuggestions({
            options,
            inputValue,
            selectedTags,
            getItemProps,
            highlightedIndex
          })}
        </MenuList>
      </Paper>
    </div>
  );
}

Suggestion.defaultProps = {
  highlightedIndex: 0
};

Suggestion.propTypes = {
  classes: PropTypes.object.isRequired,
  menuProps: PropTypes.object.isRequired,
  getItemProps: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  inputValue: PropTypes.string.isRequired,
  selectedTags: PropTypes.array.isRequired,
  highlightedIndex: PropTypes.number
};

export default Suggestion;
