import React from 'react';
import PropTypes from 'prop-types';

import { TextField } from '@material-ui/core';

function renderInput(props) {
  const { InputProps = {}, classes, myRef, ...other } = props;

  return (
    <TextField
      fullWidth
      inputRef={myRef}
      variant='outlined'
      InputProps={{
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput
        },
        ...InputProps
      }}
      style={{ width: '95%' }}
      {...other}
    />
  );
}

renderInput.propTypes = {
  classes: PropTypes.object.isRequired,
  // eslint-disable-next-line react/require-default-props
  InputProps: PropTypes.object,
  myRef: PropTypes.object.isRequired
};

export default renderInput;
