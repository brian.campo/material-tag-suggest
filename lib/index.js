import React, { Fragment, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';

import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import MTInput from './Input';
import Suggestion from './Suggestion';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: 250
  },
  container: {
    flexGrow: 1,
    position: 'relative'
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  chip: {
    margin: theme.spacing(0.5, 0.25)
  },
  inputRoot: {
    flexWrap: 'wrap'
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1
  },
  divider: {
    height: theme.spacing(2)
  }
}));

function handleDelete(props) {
  const { deletedItem, selectedTags, setSelectedTags, onDeleteTag } = props;
  const removedTag = selectedTags.find(option => option.label === deletedItem.label);
  const updatedTags = selectedTags.filter(tag => tag !== removedTag);
  setSelectedTags(updatedTags);
  onDeleteTag({ deletedTag: deletedItem, selectedTags: updatedTags });
}

function handleKeyDown(props) {
  const {
    event,
    selectedTags,
    setSelectedTags,
    setInputValue,
    inputRef,
    delimiters,
    onDeleteTag,
    onCreateTag
  } = props;

  if (event.key === 'Escape') {
    event.nativeEvent.preventDownshiftDefault = true;
    return;
  }

  if (event.key === 'Backspace' && !event.target.value && selectedTags.length > 0) {
    const deletedItem = selectedTags[selectedTags.length - 1];
    handleDelete({ deletedItem, selectedTags, setSelectedTags, onDeleteTag });
    event.nativeEvent.preventDownshiftDefault = true;
    return;
  }

  if (delimiters && delimiters.indexOf(event.key) > -1 && event.target.value) {
    event.preventDefault();
    setSelectedTags([...selectedTags, { id: -1, label: event.target.value }]);
    onCreateTag({
      createdTag: { id: -1, label: event.target.value },
      selectedTags: [...selectedTags, { id: -1, label: event.target.value }]
    });
    inputRef.current.focus();
    setInputValue('');
    return;
  }
}

function handleInputChange({ event, setInputValue }) {
  setInputValue(event.target.value);
}

const Index = props => {
  const {
    id,
    label,
    placeholder = '',
    delimiters = [],
    options,
    onAddTag,
    onDeleteTag,
    onCreateTag
  } = props;
  const classes = useStyles();

  const [selectedTags, setSelectedTags] = useState([]);
  const [inputValue, setInputValue] = useState('');
  const inputRef = useRef(null);

  delimiters.indexOf(inputValue) > -1 ? setInputValue('') : null;

  function handleChange(selectedItem, downshift) {
    setSelectedTags([...selectedTags, selectedItem]);
    onAddTag({ addedTag: selectedItem, selectedTags });
    setInputValue('');
    downshift.reset();
  }

  function renderAutoSuggest(props) {
    const {
      getLabelProps,
      getInputProps,
      getMenuProps,
      getItemProps,
      isOpen,
      highlightedIndex
    } = props;

    const { onBlur, onChange, onFocus, ...inputProps } = getInputProps({
      onKeyDown: event =>
        handleKeyDown({
          event,
          selectedTags,
          delimiters,
          inputRef,
          setSelectedTags,
          setInputValue,
          onDeleteTag,
          onCreateTag
        }),
      placeholder: placeholder
    });

    return (
      <div>
        <div className={classes.container}>
          <MTInput
            myRef={inputRef}
            classes={classes}
            label={label}
            InputLabelProps={getLabelProps()}
            InputProps={{
              startAdornment: selectedTags.map(item => (
                <Chip
                  key={item.id !== -1 ? item.id : item.label}
                  tabIndex={-1}
                  label={item.label}
                  className={classes.chip}
                  onDelete={() =>
                    handleDelete({ deletedItem: item, selectedTags, setSelectedTags, onDeleteTag })
                  }
                />
              )),
              onBlur,
              onFocus,
              onChange: event => {
                handleInputChange({ event, setInputValue });
              }
            }}
            // eslint-disable-next-line react/jsx-no-duplicate-props
            inputProps={inputProps}
          />
        </div>
        {!isOpen ? null : (
          <Suggestion
            menuProps={getMenuProps()}
            getItemProps={getItemProps}
            classes={classes}
            inputValue={inputValue}
            options={options}
            selectedTags={selectedTags}
            highlightedIndex={highlightedIndex}
          />
        )}
      </div>
    );
  }
  renderAutoSuggest.propTypes = {
    getLabelProps: PropTypes.func.isRequired,
    getInputProps: PropTypes.func.isRequired,
    getMenuProps: PropTypes.func.isRequired,
    getItemProps: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    highlightedIndex: PropTypes.number.isRequired
  };

  return (
    <Fragment>
      <Downshift
        id={id}
        inputValue={inputValue}
        onChange={handleChange}
        itemToString={item => {
          return item ? item.label : '';
        }}
      >
        {props => renderAutoSuggest(props)}
      </Downshift>
    </Fragment>
  );
};

Index.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  delimiters: PropTypes.array.isRequired,
  options: PropTypes.array.isRequired,
  onAddTag: PropTypes.func.isRequired,
  onDeleteTag: PropTypes.func.isRequired,
  onCreateTag: PropTypes.func.isRequired
};

export default Index;
