import React, { Fragment, useState } from 'react';

import MaterialTags from '../lib/index';

const options = [
  { id: 1, label: 'item1' },
  { id: 2, label: 'item2' },
  { id: 3, label: 'another option' }
];

function App() {
  const [selectedTags, setSelectedTags] = useState([]);
  return (
    <Fragment>
      <p>Select the frameworks you most like working with: </p>
      <MaterialTags
        id='material-tags-example'
        label='myInput'
        placeholder='my placeholder'
        delimiters={[',', 'Tab', 'Enter']}
        options={options}
        selectedTags={selectedTags}
        onAddTag={({ addedTag, selectedTags }) => {
          setSelectedTags([...selectedTags, addedTag]);
        }}
        onDeleteTag={({ deletedTag, selectedTags }) => {
          setSelectedTags(selectedTags);
          console.log('deletedTag', deletedTag, selectedTags);
        }}
        onCreateTag={({ createdTag, selectedTags }) => {
          console.log('createdTag', createdTag, selectedTags);
        }}
      />
    </Fragment>
  );
}

export default App;
