import { createMuiTheme } from '@material-ui/core/styles';
import { amber, blue, deepOrange } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    type: 'light',
    common: {
      white: '#f8f8ff'
    },
    primary: blue,
    secondary: amber,
    error: deepOrange
  },
  typography: {
    htmlFontSize: 14,
    fontSize: 12,
    fontFamily: ['Roboto', 'Ubuntu Condensed', 'sans-serif'],
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 600
  }
});

export default theme;
